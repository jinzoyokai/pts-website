const MonacoEditorPlugin = require('monaco-editor-webpack-plugin');

module.exports = {
  mode: 'spa',

  /*
  ** Server
  */
  modules: [
    '@nuxtjs/axios',
  ],
  server: {
    host: '0.0.0.0',
    port: 3333, // default: 3000
  },
  /*
  ** Modules
  */
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/proxy',
  ],
  /*
  ** Headers of the page
  */
  head: {
    title: 'PokemonTownship - Little town, big adventures.',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' }
    ],
    link: [
      {
        rel: 'shortcut icon',
        href: '/favicon.ico',
        type: 'image/x-icon'
      },
      {
        rel: 'icon',
        href: '/favicon.ico',
        type: 'image/x-icon'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Noto+Sans+JP|Material+Icons'
      },
      {
        rel: 'stylesheet',
        href: 'https://fonts.googleapis.com/css?family=Roboto+Condensed:300,400,700&display=swap'
      }
    ]
  },
  plugins: [
    '~/plugins/vuetify',
    '~/plugins/event-bus',
    '~/plugins/card',
    '~/plugins/clipboard',
    '~/plugins/wysiwyg',
  ],
  css: ['~/assets/style/app.styl'],
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#3B8070' },
  /*
   ** Axios module configuration
   */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    browserBaseURL: '/engine',
  },

  proxy: {
    '/engine': {
      target: 'http://localhost:3000',
      pathRewrite: {
        '^/engine': '/',
      },
    },
  },

  /*
  ** Build configuration
  */
  build: {
    extractCSS: true,
    plugins: [
      new MonacoEditorPlugin({
        // https://github.com/Microsoft/monaco-editor-webpack-plugin#options
        // Include a subset of languages support
        // Some language extensions like typescript are so huge that may impact build performance
        // e.g. Build full languages support with webpack 4.0 takes over 80 seconds
        // Languages are loaded on demand at runtime
        languages: ['css', 'html']
      })
    ]
    // extend (config, ctx) {
    //   // Run ESLint on save
    //   if (ctx.isDev && ctx.isClient) {
    //     config.module.rules.push({
    //       enforce: 'pre',
    //       test: /\.(js|vue)$/,
    //       loader: 'eslint-loader',
    //       exclude: /(node_modules)/
    //     })
    //   }
    // }
  }
}
