export const state = () => ({
  active: false,
  dialog: false,
  quickCatchEvent: {
    image: null,
  },
});

export const getters = {
  active(state) {
    return state.active;
  },
  dialog(state) {
    return state.dialog;
  },
  quickCatchEvent(state) {
    return state.quickCatchEvent;
  },
}

export const mutations = {
  setActive(state, bool) {
    state.active = bool;
  },
  setDialog(state, bool) {
    state.dialog = bool;
  },
  setQuickCatchEvent(state, obj) {
    state.quickCatchEvent = obj;
  },
}

export const actions = {
  async check({ commit }, url) {
    try {
      const { data } = await this.$axios.post('/quickCatchEvents/check', { url });

      if(typeof data === 'object') {
        commit('setQuickCatchEvent', data);
        commit('setActive', true);

        return true;
      } else {
        commit('setActive', false);

        return false;
      }
    } catch (error) {
      return false;
    }
  },
}
