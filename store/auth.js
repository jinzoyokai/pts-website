export const state = () => ({
  authUser: false
});

export const getters = {
  authUser(state) {
    return state.authUser;
  },
}


export const mutations = {
  setAuthUser(state, bool) {
    state.authUser = bool;
  },
}

export const actions = {
  async init({ commit, dispatch }) {
    try {
      const result = await dispatch('user/getUser', null, { root: true });

      if(result) {
        commit('setAuthUser', true);
        return true;
      } else {
        return false;
      }
    } catch (error) {
      return false;
    }
  },
}
