export const state = () => ({
  menuItems: []
})

export const getters = {
  menuItems (state) {
    return state.menuItems;
  }
}

export const mutations = {
  setMenuItems (state, arr) {
    state.menuItems = arr
  }
}

export const actions = {
  async getMenuItems({ commit }) {
    try {
      const menuItems = await this.$axios.$get('/menu_items') || [];
      commit('setMenuItems', menuItems);
    } catch (error) {
      console.error(error);
    }
  },
}
