export const state = () => ({
  user: {
    currency: 0,
    activityCheck: '',
    modActivityCheck: '',
    hasTemporaryPassword: false,
    inventory: [],
  }
});

export const getters = {
  user(state) {
    return state.user;
  },
}

export const mutations = {
  setUser(state, obj) {
    state.user = obj;
  },
  setPD(state, int) {
    state.user.currency = int;
  },
  setCurrency(state, {currency, newBalance}) {
    state.user[currency] = newBalance;
  },
  setModActivityCheck(state, str) {
    state.user.modActivityCheck = str;
  },
  setHasTemporaryPassword(state, bool) {
    state.user.temporaryPassword = bool;
  },
}

export const actions = {
  async getUser({ commit }) {
    try {
      const user = await this.$axios.$get('/auth/self')
      commit('setUser', user);
      return true;
    } catch (error) {
      return false;
    }
  },
}
