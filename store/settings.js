export const state = () => ({
  appSettings: []
});

export const getters = {
  appSettings(state) {
    return state.appSettings;
  },
  activityCheck(state) {
    return state.appSettings.find(({key}) => key === 'activity_check');
  },
  modActivityCheck(state) {
    return state.appSettings.find(({key}) => key === 'mod_activity_check');
  },
}

export const mutations = {
  setAppSettings(state, obj) {
    state.appSettings = obj;
  },
}

export const actions = {
  async getAppSettings({ commit }) {
    try {
      const settings = await this.$axios.$get('/settings/group/app')
      commit('setAppSettings', settings);
      return true;
    } catch (error) {
      return false;
    }
  },

}
