export const state = () => ({
  thread: {
    _id: null,
    messages: [],
  },
  dialog: false,
});

export const getters = {

}

export const mutations = {
  setThread(state, obj) {
    state.thread = obj;
  },
  setThreadMessages(state, arr) {
    state.thread.messages = arr;
  },
  setMessageDialog(state, bool) {
    state.dialog = bool;
  },
  showMessageDialog(state) {
    state.dialog = true;
  },
  hideMessageDialog(state) {
    state.dialog = false;
  },
}

export const actions = {

}
