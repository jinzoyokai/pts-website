<template>
  <div class="gallery">
    <v-layout row wrap>
      <span class="headline">Most recent submissions</span>
      <v-spacer />
      <v-btn
        color="success"
        :to="`/u/${user.username}#gallery-add`"
      >
        Add to gallery
      </v-btn>
    </v-layout>

    <isotope
      v-if="submissions.docs.length"
      ref="gallery"
      :options='galleryOptions'
      v-images-loaded:on.progress="layout"
      :list="submissions.docs"
      class="my-4"
    >
      <div
        class="gallery-submission"
        v-for="(submission, i) in submissions.docs"
        :key="`submission-${i}`"
      >
        <SubmissionCard
          :value="submission"
          hide-controls
          @clickImage="selectSubmission(submission)"
        />
      </div>
    </isotope>

    <v-layout
      justify-center
      v-if="loading"
    >
      <v-progress-circular
        indeterminate
        color="grey lighten-1"
        size="24"
      ></v-progress-circular>
    </v-layout>

    <v-layout row wrap justify-center>
      <v-pagination
        v-model="submissions.page"
        :length="submissions.totalPages"
        class="mt-3"
      ></v-pagination>
    </v-layout>

    <MediaDialog
      v-model="showDialog"
      :submission="selectedSubmission"
    />
  </div>
</template>

<script>
  import userMixins from '@/mixins/user';
  import galleryMixins from '@/mixins/gallery';
  import imagesLoaded from 'vue-images-loaded';

  import MediaDialog from '@/components/Submissions/MediaDialog';
  import SubmissionCard from '@/components/submissions/SubmissionCard';

  export default {
    components: { MediaDialog, SubmissionCard },
    mixins: [userMixins, galleryMixins],
    directives: { imagesLoaded },
    async asyncData({ $axios, params }) {
      const result = await $axios.get('/submissions', {
        params: {
          page: params.page || 1
        }
      });
      return {
        submissions: result.data,
      };
    },
    watch: {
      showDialog() {
        if(!this.showDialog) {
          history.pushState(
            {},
            null,
            `/gallery/${this.submissions.page}`
          );
        }
      },
      'submissions.page' : function() {
        history.pushState(
          {},
          null,
          `/gallery/${this.submissions.page}`
        );
        this.get()
      }
    },
    data() {
      return {
        showDialog: false,
        selectedSubmission: {
          image: null,
        },
      }
    },
    computed: { },
    methods: {
      async get() {
        this.loading = true;
        this.submissions.docs = [];

        const result = await this.$axios.get('/submissions', {
          params: {
            page: this.submissions.page
          }
        });

        this.submissions = result.data;
        this.loading = false;
      },
      selectSubmission(submission) {
        history.pushState(null, null, `/u/${submission.user.username}/#gallery-${submission._id}`);
        this.selectedSubmission = JSON.parse(JSON.stringify(submission));

        this.showDialog = true;
      },
    },
  }
</script>
<style lang="stylus" scoped>
  .gallery
    .gallery-submission
      max-width: 29% !important;
      margin: 12px 1%;
</style>
