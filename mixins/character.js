import { mapGetters } from 'vuex';

export default {
  computed: {
    ...mapGetters(
      'user',
      ['user'],
    ),
    noImageUri() {
      return 'https://trainer-3.s3.amazonaws.com/1562451512393.png';
    }
  },
}
