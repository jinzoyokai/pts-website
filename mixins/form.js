export default {
  data() {
    return {
      serverErrors: [],
      valid: false,
    }
  },
  methods: {
    createPayload(model) {
      const payload = JSON.parse(JSON.stringify(model));

      Object.keys(payload).forEach(key => {
        if(payload[key] === null)
          delete payload[key]
      });

      return payload
    },
    createFormData(payload) {
      const formData = new FormData();

      Object.keys(payload).forEach(key => {
        if(!!Array.isArray(payload[key])) {
          const arrKey = `${key}[]`;

          payload[key].forEach(value => {
            formData.append(arrKey, value);
          });
        } else {
          formData.append(key, payload[key]);
        }
      });

      return formData;
    },
    toggleDialog() {
      this.$emit('input', !this.value);
      this.$emit('close');

      this.reset();
    },
    reset() {
      this.serverErrors = [];
      this.valid = false;
    }
  }
}
