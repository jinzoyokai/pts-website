import moment from 'moment';

export default {
  data() {
    const page = this.$route.query.page || 1;
    return {
      page,
      totalPages: null,
    }
  },
  watch: {
    page() {
      this.$router.push({query: {page: this.page}})
      this.paginationHandler();
    }
  },
  methods: {
    paginationHandler() {
      console.log('Pagination handler triggered.')
    },
  }
}
