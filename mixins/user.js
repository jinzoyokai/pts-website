import { mapGetters } from 'vuex';

export default {
  data() {
    return {
      permissions: [
        {
          value: 'admin',
          label: 'Admin'
        },
        {
          value: 'moderate',
          label: 'Moderate'
        },
        {
          value: 'author',
          label: 'Author'
        },
        {
          value: 'member',
          label: 'Member'
        },
        {
          value: 'can_edit_users',
          label: 'Can edit users'
        },
        {
          value: 'can_edit_items',
          label: 'Can edit items'
        },
        {
          value: 'can_edit_companions',
          label: 'Can edit companions'
        },
        {
          value: 'can_edit_characters',
          label: 'Can edit characters'
        },
        {
          value: 'can_edit_sections',
          label: 'Can edit sections'
        },
        {
          value: 'can_edit_shops',
          label: 'Can edit shops'
        },
        {
          value: 'can_edit_ranks',
          label: 'Can edit ranks'
        },
        {
          value: 'can_edit_trades',
          label: 'Can edit trades'
        },
        {
          value: 'can_edit_submissions',
          label: 'Can edit submissions'
        },
        {
          value: 'can_edit_quickcatchevents',
          label: 'Can edit quick catch events'
        },
        {
          value: 'can_edit_trade_ins',
          label: 'Can edit trade ins'
        },
      ]
    };
  },
  methods: {
    isUser({user}) {
      if(!user) return false;

      const userId = user._id || user;
      return !!userId && userId === this.user._id;
    },
  },
  computed: {
    ...mapGetters(
      'auth',
      ['authUser'],
    ),
    ...mapGetters(
      'user',
      ['user'],
    ),
    userInventory() {
      return this.user.inventory;
    },
    isAdmin() {
      return this.user && this.user._id && this.user.permissions.indexOf('admin') !== -1;
    },
    canModerate() {
      return this.user && this.user._id && this.user.permissions.indexOf('moderate') !== -1;
    },
    canEditUsers() {
      return this.user && this.user._id && this.user.permissions.indexOf('can_edit_users') !== -1;
    },
    canEditCharacters() {
      return this.user && this.user._id && this.user.permissions.indexOf('can_edit_characters') !== -1;
    },
    canEditSections() {
      return this.user && this.user._id && this.user.permissions.indexOf('can_edit_sections') !== -1;
    },
    canEditSubmissions() {
      return this.user && this.user._id && this.user.permissions.indexOf('can_edit_submissions') !== -1;
    },
    authUserProfileLink() {
      if(!!this.authUser)
        return `/profile/${this.user._id}`;

      return '/register';
    },
  },
}
