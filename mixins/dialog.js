export default {
  methods: {
    onError() {
      this.$bus.$emit('error', {
        message: 'There was an error. Please try again.'
      });
    },
  }
}
