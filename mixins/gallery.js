import isotope from 'vueisotope';
import imagesLoaded from 'vue-images-loaded';

export default {
  components: { isotope },
  directives: { imagesLoaded },
  data() {
    return {
      showAllSubmissions: false,
      galleryOptions: {
        itemSelector: '.submission',
        masonry: {
          columnWidth: 12,
          gutterWidth: 12
        }
      },
    }
  },
  methods: {
    layout() {
      setTimeout(() => {
        this.$refs.gallery.layout('masonry');
      }, 500);
    },
    onChangeVisibility(hidden) {
      this.get();

      let message;

      if(!!hidden) {
        message = 'Submission has been hidden';
      } else {
        message = 'Submission has been made public';
      }

      this.$bus.$emit('success', { message });
    },
    onApproveSubmission() {
      this.get();
      this.$bus.$emit('success', {
        message: `Submission has been approved.`
      });
    },
    onDeclineSubmission() {
      this.get();
      this.$bus.$emit('error', {
        message: `Submission has been declined.`
      });
    },
    onDeleteSubmission() {
      this.get();
      this.$bus.$emit('error', {
        message: `Submission has been deleted.`
      });
    }
  }
}
