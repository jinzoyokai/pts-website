import moment from 'moment';

export default {
  methods: {
    hoomenTime(dateTime) {
      return moment(dateTime).fromNow();
    },
    dateTime(dateTime) {
      return moment(dateTime).format('MM/DD/YYYY | hh:mm a');
    }
  }
}
