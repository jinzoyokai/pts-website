export default {
  methods: {
    menuItemClick({url, href, to}) {
      if(!!href) {
        window.open(href, '_blank');
      } else if(!!url) {
        if(url.startsWith('http')) {
          window.open(url, '_blank');
        } else {
          if(url.includes('{username}'))
            url = url.replace('{username}', this.$store.state.user.user.username);

          this.$router.push(url);
        }
      } else {
        this.$router.push(to);
      }
    },
  },
}
