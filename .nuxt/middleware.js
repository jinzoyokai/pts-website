const middleware = {}

middleware['activityCheck'] = require('@/middleware/activityCheck.js');
middleware['activityCheck'] = middleware['activityCheck'].default || middleware['activityCheck']

middleware['auth'] = require('@/middleware/auth.js');
middleware['auth'] = middleware['auth'].default || middleware['auth']

middleware['check-auth'] = require('@/middleware/check-auth.js');
middleware['check-auth'] = middleware['check-auth'].default || middleware['check-auth']

middleware['modActivityCheck'] = require('@/middleware/modActivityCheck.js');
middleware['modActivityCheck'] = middleware['modActivityCheck'].default || middleware['modActivityCheck']

middleware['moderate'] = require('@/middleware/moderate.js');
middleware['moderate'] = middleware['moderate'].default || middleware['moderate']

middleware['quickCatchEvent'] = require('@/middleware/quickCatchEvent.js');
middleware['quickCatchEvent'] = middleware['quickCatchEvent'].default || middleware['quickCatchEvent']

export default middleware
