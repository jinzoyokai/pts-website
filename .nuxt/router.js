import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'

const _5590113a = () => interopDefault(import('..\\pages\\activity.vue' /* webpackChunkName: "pages_activity" */))
const _7c596b84 = () => interopDefault(import('..\\pages\\activitycheck.vue' /* webpackChunkName: "pages_activitycheck" */))
const _5ab7f5ce = () => interopDefault(import('..\\pages\\admin\\index.vue' /* webpackChunkName: "pages_admin_index" */))
const _1d07e576 = () => interopDefault(import('..\\pages\\contact.vue' /* webpackChunkName: "pages_contact" */))
const _694ec5f6 = () => interopDefault(import('..\\pages\\forums\\index.vue' /* webpackChunkName: "pages_forums_index" */))
const _b7476964 = () => interopDefault(import('..\\pages\\login.vue' /* webpackChunkName: "pages_login" */))
const _343e26f5 = () => interopDefault(import('..\\pages\\logout.vue' /* webpackChunkName: "pages_logout" */))
const _3e1da6c2 = () => interopDefault(import('..\\pages\\modactivitycheck.vue' /* webpackChunkName: "pages_modactivitycheck" */))
const _536ca87e = () => interopDefault(import('..\\pages\\news.vue' /* webpackChunkName: "pages_news" */))
const _1454304e = () => interopDefault(import('..\\pages\\settings.vue' /* webpackChunkName: "pages_settings" */))
const _0e303420 = () => interopDefault(import('..\\pages\\shops\\index.vue' /* webpackChunkName: "pages_shops_index" */))
const _7a607b0c = () => interopDefault(import('..\\pages\\trades.vue' /* webpackChunkName: "pages_trades" */))
const _5fae45e1 = () => interopDefault(import('..\\pages\\admin\\boards\\index.vue' /* webpackChunkName: "pages_admin_boards_index" */))
const _00da795c = () => interopDefault(import('..\\pages\\admin\\color_statuses\\index.vue' /* webpackChunkName: "pages_admin_color_statuses_index" */))
const _35c78c26 = () => interopDefault(import('..\\pages\\admin\\content\\index.vue' /* webpackChunkName: "pages_admin_content_index" */))
const _784c2844 = () => interopDefault(import('..\\pages\\admin\\gacha\\index.vue' /* webpackChunkName: "pages_admin_gacha_index" */))
const _44fdc974 = () => interopDefault(import('..\\pages\\admin\\items\\index.vue' /* webpackChunkName: "pages_admin_items_index" */))
const _7189555a = () => interopDefault(import('..\\pages\\admin\\pokemon\\index.vue' /* webpackChunkName: "pages_admin_pokemon_index" */))
const _2ead1f67 = () => interopDefault(import('..\\pages\\admin\\quickCatchEvents\\index.vue' /* webpackChunkName: "pages_admin_quickCatchEvents_index" */))
const _341f0edf = () => interopDefault(import('..\\pages\\admin\\ranks\\index.vue' /* webpackChunkName: "pages_admin_ranks_index" */))
const _3b704d4b = () => interopDefault(import('..\\pages\\admin\\settings\\index.vue' /* webpackChunkName: "pages_admin_settings_index" */))
const _151dbea9 = () => interopDefault(import('..\\pages\\admin\\shops\\index.vue' /* webpackChunkName: "pages_admin_shops_index" */))
const _7ef1c8ff = () => interopDefault(import('..\\pages\\admin\\submissions\\index.vue' /* webpackChunkName: "pages_admin_submissions_index" */))
const _daf03e90 = () => interopDefault(import('..\\pages\\admin\\submissionTool.vue' /* webpackChunkName: "pages_admin_submissionTool" */))
const _67e33324 = () => interopDefault(import('..\\pages\\admin\\tradeIns\\index.vue' /* webpackChunkName: "pages_admin_tradeIns_index" */))
const _44b77b5f = () => interopDefault(import('..\\pages\\admin\\trades\\index.vue' /* webpackChunkName: "pages_admin_trades_index" */))
const _12c19431 = () => interopDefault(import('..\\pages\\admin\\trainer\\index.vue' /* webpackChunkName: "pages_admin_trainer_index" */))
const _81e99004 = () => interopDefault(import('..\\pages\\admin\\users\\index.vue' /* webpackChunkName: "pages_admin_users_index" */))
const _62f917cc = () => interopDefault(import('..\\pages\\games\\gacha.vue' /* webpackChunkName: "pages_games_gacha" */))
const _5d1dceff = () => interopDefault(import('..\\pages\\games\\slots.vue' /* webpackChunkName: "pages_games_slots" */))
const _30bf94f6 = () => interopDefault(import('..\\pages\\admin\\settings\\home.vue' /* webpackChunkName: "pages_admin_settings_home" */))
const _29bd7576 = () => interopDefault(import('..\\pages\\admin\\settings\\menu.vue' /* webpackChunkName: "pages_admin_settings_menu" */))
const _6b5d2305 = () => interopDefault(import('..\\pages\\admin\\users\\activitycheck.vue' /* webpackChunkName: "pages_admin_users_activitycheck" */))
const _5b031b0c = () => interopDefault(import('..\\pages\\admin\\users\\staff.vue' /* webpackChunkName: "pages_admin_users_staff" */))
const _39e0a232 = () => interopDefault(import('..\\pages\\admin\\roller\\travel\\_id.vue' /* webpackChunkName: "pages_admin_roller_travel__id" */))
const _4bea5d5c = () => interopDefault(import('..\\pages\\alerts\\_page.vue' /* webpackChunkName: "pages_alerts__page" */))
const _08e9e472 = () => interopDefault(import('..\\pages\\c\\_slug.vue' /* webpackChunkName: "pages_c__slug" */))
const _7de20b35 = () => interopDefault(import('..\\pages\\forums\\_id\\index.vue' /* webpackChunkName: "pages_forums__id_index" */))
const _43ab1dc7 = () => interopDefault(import('..\\pages\\gallery\\_page.vue' /* webpackChunkName: "pages_gallery__page" */))
const _30570e71 = () => interopDefault(import('..\\pages\\learn\\_slug.vue' /* webpackChunkName: "pages_learn__slug" */))
const _dbcda772 = () => interopDefault(import('..\\pages\\messages\\_page.vue' /* webpackChunkName: "pages_messages__page" */))
const _eb0682f0 = () => interopDefault(import('..\\pages\\shops\\_id.vue' /* webpackChunkName: "pages_shops__id" */))
const _1b1e057c = () => interopDefault(import('..\\pages\\trade_ins\\_slug.vue' /* webpackChunkName: "pages_trade_ins__slug" */))
const _63293760 = () => interopDefault(import('..\\pages\\trainer\\_id.vue' /* webpackChunkName: "pages_trainer__id" */))
const _a893c16a = () => interopDefault(import('..\\pages\\u\\_username.vue' /* webpackChunkName: "pages_u__username" */))
const _1d3371e6 = () => interopDefault(import('..\\pages\\forums\\_id\\threads\\_thread.vue' /* webpackChunkName: "pages_forums__id_threads__thread" */))
const _4a96af92 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

if (process.client) {
  if ('scrollRestoration' in window.history) {
    window.history.scrollRestoration = 'manual'

    // reset scrollRestoration to auto when leaving page, allowing page reload
    // and back-navigation from other pages to use the browser to restore the
    // scrolling position.
    window.addEventListener('beforeunload', () => {
      window.history.scrollRestoration = 'auto'
    })

    // Setting scrollRestoration to manual again when returning to this page.
    window.addEventListener('load', () => {
      window.history.scrollRestoration = 'manual'
    })
  }
}
const scrollBehavior = function (to, from, savedPosition) {
  // if the returned position is falsy or an empty object,
  // will retain current scroll position.
  let position = false

  // if no children detected and scrollToTop is not explicitly disabled
  if (
    to.matched.length < 2 &&
    to.matched.every(r => r.components.default.options.scrollToTop !== false)
  ) {
    // scroll to the top of the page
    position = { x: 0, y: 0 }
  } else if (to.matched.some(r => r.components.default.options.scrollToTop)) {
    // if one of the children has scrollToTop option set to true
    position = { x: 0, y: 0 }
  }

  // savedPosition is only available for popstate navigations (back button)
  if (savedPosition) {
    position = savedPosition
  }

  return new Promise((resolve) => {
    // wait for the out transition to complete (if necessary)
    window.$nuxt.$once('triggerScroll', () => {
      // coords will be used if no selector is provided,
      // or if the selector didn't match any element.
      if (to.hash) {
        let hash = to.hash
        // CSS.escape() is not supported with IE and Edge.
        if (typeof window.CSS !== 'undefined' && typeof window.CSS.escape !== 'undefined') {
          hash = '#' + window.CSS.escape(hash.substr(1))
        }
        try {
          if (document.querySelector(hash)) {
            // scroll to anchor by returning the selector
            position = { selector: hash }
          }
        } catch (e) {
          console.warn('Failed to save scroll position. Please add CSS.escape() polyfill (https://github.com/mathiasbynens/CSS.escape).')
        }
      }
      resolve(position)
    })
  })
}

export function createRouter() {
  return new Router({
    mode: 'history',
    base: decodeURI('/'),
    linkActiveClass: 'nuxt-link-active',
    linkExactActiveClass: 'nuxt-link-exact-active',
    scrollBehavior,

    routes: [{
      path: "/activity",
      component: _5590113a,
      name: "activity"
    }, {
      path: "/activitycheck",
      component: _7c596b84,
      name: "activitycheck"
    }, {
      path: "/admin",
      component: _5ab7f5ce,
      name: "admin"
    }, {
      path: "/contact",
      component: _1d07e576,
      name: "contact"
    }, {
      path: "/forums",
      component: _694ec5f6,
      name: "forums"
    }, {
      path: "/login",
      component: _b7476964,
      name: "login"
    }, {
      path: "/logout",
      component: _343e26f5,
      name: "logout"
    }, {
      path: "/modactivitycheck",
      component: _3e1da6c2,
      name: "modactivitycheck"
    }, {
      path: "/news",
      component: _536ca87e,
      name: "news"
    }, {
      path: "/settings",
      component: _1454304e,
      name: "settings"
    }, {
      path: "/shops",
      component: _0e303420,
      name: "shops"
    }, {
      path: "/trades",
      component: _7a607b0c,
      name: "trades"
    }, {
      path: "/admin/boards",
      component: _5fae45e1,
      name: "admin-boards"
    }, {
      path: "/admin/color_statuses",
      component: _00da795c,
      name: "admin-color_statuses"
    }, {
      path: "/admin/content",
      component: _35c78c26,
      name: "admin-content"
    }, {
      path: "/admin/gacha",
      component: _784c2844,
      name: "admin-gacha"
    }, {
      path: "/admin/items",
      component: _44fdc974,
      name: "admin-items"
    }, {
      path: "/admin/pokemon",
      component: _7189555a,
      name: "admin-pokemon"
    }, {
      path: "/admin/quickCatchEvents",
      component: _2ead1f67,
      name: "admin-quickCatchEvents"
    }, {
      path: "/admin/ranks",
      component: _341f0edf,
      name: "admin-ranks"
    }, {
      path: "/admin/settings",
      component: _3b704d4b,
      name: "admin-settings"
    }, {
      path: "/admin/shops",
      component: _151dbea9,
      name: "admin-shops"
    }, {
      path: "/admin/submissions",
      component: _7ef1c8ff,
      name: "admin-submissions"
    }, {
      path: "/admin/submissionTool",
      component: _daf03e90,
      name: "admin-submissionTool"
    }, {
      path: "/admin/tradeIns",
      component: _67e33324,
      name: "admin-tradeIns"
    }, {
      path: "/admin/trades",
      component: _44b77b5f,
      name: "admin-trades"
    }, {
      path: "/admin/trainer",
      component: _12c19431,
      name: "admin-trainer"
    }, {
      path: "/admin/users",
      component: _81e99004,
      name: "admin-users"
    }, {
      path: "/games/gacha",
      component: _62f917cc,
      name: "games-gacha"
    }, {
      path: "/games/slots",
      component: _5d1dceff,
      name: "games-slots"
    }, {
      path: "/admin/settings/home",
      component: _30bf94f6,
      name: "admin-settings-home"
    }, {
      path: "/admin/settings/menu",
      component: _29bd7576,
      name: "admin-settings-menu"
    }, {
      path: "/admin/users/activitycheck",
      component: _6b5d2305,
      name: "admin-users-activitycheck"
    }, {
      path: "/admin/users/staff",
      component: _5b031b0c,
      name: "admin-users-staff"
    }, {
      path: "/admin/roller/travel/:id?",
      component: _39e0a232,
      name: "admin-roller-travel-id"
    }, {
      path: "/alerts/:page?",
      component: _4bea5d5c,
      name: "alerts-page"
    }, {
      path: "/c/:slug?",
      component: _08e9e472,
      name: "c-slug"
    }, {
      path: "/forums/:id",
      component: _7de20b35,
      name: "forums-id"
    }, {
      path: "/gallery/:page?",
      component: _43ab1dc7,
      name: "gallery-page"
    }, {
      path: "/learn/:slug?",
      component: _30570e71,
      name: "learn-slug"
    }, {
      path: "/messages/:page?",
      component: _dbcda772,
      name: "messages-page"
    }, {
      path: "/shops/:id",
      component: _eb0682f0,
      name: "shops-id"
    }, {
      path: "/trade_ins/:slug?",
      component: _1b1e057c,
      name: "trade_ins-slug"
    }, {
      path: "/trainer/:id?",
      component: _63293760,
      name: "trainer-id"
    }, {
      path: "/u/:username?",
      component: _a893c16a,
      name: "u-username"
    }, {
      path: "/forums/:id/threads/:thread?",
      component: _1d3371e6,
      name: "forums-id-threads-thread"
    }, {
      path: "/",
      component: _4a96af92,
      name: "index"
    }],

    fallback: false
  })
}
