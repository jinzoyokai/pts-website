import Vue from 'vue';
import wysiwyg from "@/lib/editor";

Vue.use(wysiwyg, {
  image: {
    uploadURL: "/engine/upload",
    dropzoneOptions: {}
  },
});
