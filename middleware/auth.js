export default function ({ store, error }) {
  if (!store.getters['auth/authUser']) {
    error({
      message: 'You are not logged in.',
      statusCode: 403
    })
  }
}
