export default async function ({ $axios, route, store }) {
  if(!!store.getters['auth/authUser']) {
    const url = route.fullPath;

    await store.dispatch('quickCatchEvent/check', url);
  }
}
