export default async function ({ store, error }) {
  let activityCheck = store.getters['settings/activityCheck'];


  if(activityCheck === undefined)
    await store.dispatch('settings/getAppSettings');

  activityCheck = store.getters['settings/activityCheck'];

  if (!activityCheck || !activityCheck.value) {
    error({
      message: 'Activity checks are not open.',
      statusCode: 403
    })
  }
}
