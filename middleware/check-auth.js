export default async function (context) {
  await context.store.dispatch('auth/init', context.req, { root: true });

  if (context.store.getters['auth/authUser']) {
    const user = context.store.getters['user/user'];

    if(
      user.hasTemporaryPassword
      && context.route.path !== '/settings'
    ) {
      window.location.href = '/settings';
    }
  }
}
