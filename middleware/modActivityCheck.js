export default async function ({ store, error }) {
  let modActivityCheck = store.getters['settings/modActivityCheck'];


  if(modActivityCheck === undefined)
    await store.dispatch('settings/getAppSettings');

  modActivityCheck = store.getters['settings/modActivityCheck'];

  if (!modActivityCheck || !modActivityCheck.value) {
    error({
      message: 'Activity checks are not open.',
      statusCode: 403
    })
  }
}
