export default function ({ store, error }) {
  const user = store.getters['user/user']

  if (!user) {
    error({
      message: 'You are not logged in.',
      statusCode: 403
    })
  }

  if (
    !user.permissions
    || user.permissions.indexOf('moderate') === -1
  ) {
    error({
      message: 'You are not a moderator.',
      statusCode: 403
    })
  }
}
